'use strict';

import $ from 'jquery';
import ko from 'knockout';
import koScrollbar from 'knockout-scrollbar';
import koSortable from 'knockout-sortable';
import {setState} from 'knockout-store';
import projects from './fake-model/fake-projects';
import './components/app';

window.ko = ko;
ko.deferUpdates = true;

const state = {
    projects: ko.observableArray([]),
    selectedProject: ko.observable(),
};

setState(state);

setTimeout(() => {
    const observableProjects = projects.map(({name, statuses}) => {
        return {
            name: ko.observable(name),
            statuses: ko.observableArray(statuses),
        };
    });
    state.projects(observableProjects);
}, 1000);

ko.applyBindings();