import faker from 'faker';

export default function getFakeTasks(limit = 10) {
    let data = [];
    let rand = Math.floor(Math.random() * limit);

    for (let i = 0; i <= rand; i++) {
        data.push({
            title: `${faker.hacker.noun()} ${faker.hacker.ingverb()}`,
            description: faker.hacker.phrase()
        });
    }

    return data;
}