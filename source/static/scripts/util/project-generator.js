import faker from 'faker';

export default function getFakeProjects(limit = 5) {
    let data = [];
    let rand = Math.floor(Math.random() * limit + 1);

    for (let i = 0; i <= rand; i++) {
        data.push({
            name: faker.company.companyName(),
        });
    }

    return data;
}