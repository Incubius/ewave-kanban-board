import ko from 'knockout';
import {connect} from 'knockout-store';

function projectListViewModel(params) {
    const vm = {};
    vm.projects = params.projects;
    vm.activeIndex = ko.observable();

    function selectProject(selectedProject, event) {
        let context = ko.contextFor(event.currentTarget);
        if (context) {
            vm.activeIndex(context.$index());
        }
        params.selectedProject(selectedProject);
    }
    vm.selectProject = selectProject;

    return vm;
}

function mapStateToParams({selectedProject, projects}) {
    return {selectedProject, projects};
}

export default connect(mapStateToParams)(projectListViewModel);