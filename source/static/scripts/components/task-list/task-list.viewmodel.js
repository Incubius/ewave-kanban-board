import ko from 'knockout';

function taskListViewModel(params) {
    const vm = {};

    vm.tasks = ko.observableArray(params.tasks);
    vm.index = ko.observable(params.index);
    vm.activeIndex = ko.observable();

    function setActiveProject($index) {
        if (typeof $index === 'undefined' || $index === vm.activeIndex()) {
            vm.activeIndex(null);
            return;
        }
        vm.activeIndex($index);
    }

    vm.setActiveProject = setActiveProject;

    return vm;
}

export default taskListViewModel;