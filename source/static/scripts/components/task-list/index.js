import ko from 'knockout';
import viewModel from './task-list.viewmodel.js';
import template from './task-list.template.html';

ko.components.register('task-list', {viewModel, template});