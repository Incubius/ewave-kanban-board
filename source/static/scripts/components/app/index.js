import ko from 'knockout';
import template from './app.template.html';
import viewModel from './app.viewmodel.js';
import '../project-list';
import '../board';
import '../task-list';
import '../add-task';

ko.components.register('app', {template, viewModel});