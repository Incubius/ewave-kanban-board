import ko from 'knockout';
import {connect} from 'knockout-store';

function boardViewModel(params) {
    const vm = {};
    vm.projectSelected = ko.computed(() => typeof params.selectedProject() !== 'undefined');
    vm.statuses = ko.computed(() => {
        if (!vm.projectSelected()) {
            return [];
        }
        return params.selectedProject().statuses();
    });
    vm.statusEmpty = ko.computed(() => vm.statuses().length === 0);
    return vm;
}

function mapStateToParams({selectedProject}) {
    return {selectedProject};
}

export default connect(mapStateToParams)(boardViewModel);
