import ko from 'knockout';
import viewModel from './board.viewmodel';
import template from './board.template.html';

ko.components.register('board', {viewModel, template});
