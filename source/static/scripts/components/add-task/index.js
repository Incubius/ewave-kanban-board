import ko from 'knockout';
import viewModel from './add-task.viewmodel';
import template from './add-task.template.html';

ko.components.register('add-task', { viewModel, template });
