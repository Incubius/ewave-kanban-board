import ko from 'knockout';

function addTaskViewModel(params) {
    const vm = {};
    vm.isActive = ko.observable(false);
    vm.taskTitle = ko.observable();
    vm.taskDescription = ko.observable();

    function addTask() {
        if (typeof vm.taskTitle() !== 'undefined' && vm.taskTitle().trim() !== '') {
            params.taskList.push({title: vm.taskTitle(), description: vm.taskDescription()});
            vm.taskTitle('');
            vm.taskDescription('');
            vm.toggleForm();
        }
    }

    function toggleForm() {
        vm.isActive(!vm.isActive());
    }

    vm.addTaskClick = addTask;
    vm.toggleForm = toggleForm;
    return vm;
}

export default addTaskViewModel;
