import {statuses} from './fake-statuses';
import getFakeProjects from '../util/project-generator';
import getFakeTasks from '../util/task-generator';

const projects = getFakeProjects();

projects.forEach(function (project) {
    project.statuses = [];
    statuses.forEach(function (status) {
        project.statuses[status.id] = {};
        project.statuses[status.id]['tasks'] = getFakeTasks();
        project.statuses[status.id]['label'] = status.label;
    });
});

export default projects;