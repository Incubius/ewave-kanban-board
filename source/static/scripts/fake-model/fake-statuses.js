export const STATUS_OPEN = 0;
export const STATUS_BA = 1;
export const STATUS_BE = 2;
export const STATUS_FE = 3;
export const STATUS_QA = 4;
export const STATUS_MTL = 5;
export const STATUS_DONE = 6;
export const statuses = [
    {
        id: STATUS_OPEN,
        label: 'Open'
    }, {
        id: STATUS_BA,
        label: 'BA'
    }, {
        id: STATUS_BE,
        label: 'Develop Back-end'
    }, {
        id: STATUS_FE,
        label: 'Develop Front-end'
    }, {
        id: STATUS_QA,
        label: 'QA'
    }, {
        id: STATUS_MTL,
        label: 'Move To Live'
    }, {
        id: STATUS_DONE,
        label: 'Done'
    }
];