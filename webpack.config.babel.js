import path from 'path';
import webpack from 'webpack';

import {NODE_ENV, isDevelopment} from './gulp/util/env.js';

const outputFileName = '[name].js';

let options = {
    entry: {
        vendor: [
            'jquery',
            'knockout',
            './vendor.js'
        ],
        main: './main.js'
    },
    output: {
        filename: outputFileName,
        path: __dirname + '/dest/assets/javascripts',
        publicPath: '/assets/javascripts/',
        library: '[name]'
    },
    mode: NODE_ENV,
    devtool: isDevelopment ? 'eval-source-map' : 'source-map',
    context: path.resolve(__dirname, 'source/static/scripts'),
    module: {
        noParse: /\/node_modules\/(jquery)/,
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ['babel-loader','eslint-loader']
            },
            {
                test: /\.html/,
                use: {
                    loader: "html-loader",
                    options: { removeComments: false }
                }
            }
        ]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    name: 'vendor',
                    minChunks: Infinity,
                    filename: outputFileName
                }
            }
        }
    }
};

options.plugins = [
    new webpack.DefinePlugin({
        NODE_ENV: JSON.stringify(NODE_ENV),
        'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
    }),
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
    })
];

if (isDevelopment) {
    options.plugins.push(
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    );
}
else {
    options.plugins.push(
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        })
    );
}

export default options;