import gulp from "gulp";

import { reload } from "./server";
import { pages, html } from "./html";
import css from "./css";
import scripts from "./scripts";

const watch = () => {
  global.watch = true;

  // Modules, pages
  gulp
    .watch("source/**/*.html", gulp.series(pages, reload))
    .on("all", (event, filepath) => {
      global.emittyChangedFile = filepath;
    });

  // Static styles
  gulp.watch("source/static/styles/**/*.styl", gulp.series(css, reload));

  // Modules styles
  gulp.watch("source/static/scripts/components/**/*.styl", gulp.series(css, reload));

  // Static scripts
  gulp.watch("source/static/scripts/**/*.js", gulp.series(scripts, reload));

  // Static scripts template
  gulp.watch("source/static/scripts/**/*.html", gulp.series(scripts, reload));

  // Modules scripts
  gulp.watch("source/modules/*/*.js", gulp.series(scripts, reload));
};

export default watch;
