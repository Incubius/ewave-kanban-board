import gulp from 'gulp';
import plumber from 'gulp-plumber';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import gulplog from "gulplog";

import webpackConfig from '../../webpack.config.babel';
import {plumberConfig} from '../config/plumber';

const scripts = callback => {
    let firstBuildReady = false;
    
    function done(err, stats) {
        firstBuildReady = true;
        
        if (err) {
            return;
        }

        gulplog[stats.hasErrors() ? 'error' : 'info'](
            stats.toString({
                chunks: false,
                modules: false,
                colors: true
            })
        );
    }

    return gulp.src(['*.js', '!_*.js'], {cwd: 'source/static/scripts'})
               .pipe(plumber(plumberConfig))
               .pipe(webpackStream(webpackConfig, webpack, done))
               .pipe(gulp.dest('dest/assets/js'))
               .on("data", () => {
                   if (firstBuildReady) {
                       callback();
                   }
               });
};

export default scripts;