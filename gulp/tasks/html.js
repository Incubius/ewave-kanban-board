import gulp from "gulp";
import plumber from "gulp-plumber";
import posthtml from "gulp-posthtml";
import prettify from "gulp-prettify";

import {plumberConfig} from "../config/plumber";
import {posthtmlConfig} from "../config/posthtml";
import {htmlPrettifyConfig} from "../config/html-prettify";

export function pages() {
    return gulp.src(["**/*.html"], {cwd: "source/pages"})
               .pipe(plumber(plumberConfig))
               .pipe(posthtml(posthtmlConfig.plugins, posthtmlConfig.options))
               .pipe(prettify(htmlPrettifyConfig))
               .pipe(gulp.dest("dest"));
}

export const html = gulp.series(pages);