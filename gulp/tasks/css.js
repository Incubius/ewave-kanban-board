import gulp from "gulp";
import plumber from "gulp-plumber";
import stylus from "gulp-stylus";
import combineMq from "gulp-combine-mq";
import postcss from "gulp-postcss";

import {plumberConfig} from "../config/plumber";
import {stylusConfig} from "../config/stylus";

const css = () =>
    gulp
        .src(["*.styl", "!_*.styl"], { cwd: "source/static/styles" })
        .pipe(plumber(plumberConfig))
        .pipe(stylus(stylusConfig))
        .pipe(combineMq({ beautify: true }))
        .pipe(postcss())
        .pipe(gulp.dest("dest/assets/css"));

export default css;