import {series, parallel} from 'gulp';

import cleanup from "./cleanup";
import {html} from "./html";
import scripts from './scripts';
import css from "./css";

const build = series(
    cleanup,
    series(
        parallel(html, scripts),
        css
    )
);

export default build;