import browserSync from "../util/getBrowserSyncInstance";
import { browserSyncConfig } from "../config/browser-sync";

export const server = () => browserSync.init(browserSyncConfig);

export const reload = callback => {
  browserSync.reload();
  callback();
};
