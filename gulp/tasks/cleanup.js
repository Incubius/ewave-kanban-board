import del from "del";

import {delConfig} from "../config/del";

const cleanup = callback => del(delConfig, callback);

export default cleanup;