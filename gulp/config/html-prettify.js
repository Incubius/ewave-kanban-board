export const htmlPrettifyConfig = {
    unformatted: ["pre", "code", "textarea", "script"],
    indent_char: " ",
    indent_size: 2,
    preserve_newlines: true,
    brace_style: "expand",
    end_with_newline: true
};