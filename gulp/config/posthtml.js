import posthtmlAttrsSorter from "posthtml-attrs-sorter";

export const posthtmlConfig = {
    plugins: [
        posthtmlAttrsSorter({
            order: [
                "id",
                "class",
                "name",
                "data",
                "src",
                "for",
                "type",
                "href",
                "values",
                "title",
                "alt",
                "role",
                "aria"
            ]
        })
    ],
    options: {}
};