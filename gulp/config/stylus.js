import path from "path";

const CWD = process.cwd();

import rupture from "rupture";
import stylusFileExists from "../util/stylusFileExists";

export const stylusConfig = {
    use: [rupture(), stylusFileExists()],
    include: [path.join(CWD, "node_modules")],
    "include css": true
};